## Sobre o Projeto

Bolierplate com laravel 5.8 e adminlte3, policies(sem configurar ainda) com auto gestão de perfil e manter usuários

## Como Instalar
1. `git clone`  
1. `php composer install`
1. `cp .env.example .env` e configure dados de ambiente local(APP\_URL, DB\_\*, MAIL\_*)
1. `php artisan key:generate`
1. `npm install` or `yarn install`
1. `npm run dev` or `yarn run dev`
1. `php artisan migrate:fresh --seed`

 ```sh
 echo 'We can do it!'
 ```

## Pacotes Laravel Utilizados
* [Laravel-AdminLTE 3](https://github.com/ColorlibHQ/AdminLTE/releases/tag/v3.0.0-beta.2)
* [Laravel-Permission](https://github.com/spatie/laravel-permission) not used yet
* [Laravel-5-Generators-Extended](https://github.com/laracasts/Laravel-5-Generators-Extended)
* [LaravelCollective/HTML](https://github.com/LaravelCollective/docs/blob/5.6/html.md)
* [Laravel-Medialibrary](https://github.com/spatie/laravel-medialibrary) not used yet
* [Laravel-Migrations-Generator](https://github.com/Xethron/migrations-generator)
* [Laravel-Iseed](https://github.com/orangehill/iseed)
